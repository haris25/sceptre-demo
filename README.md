# README

Prerequisite:

- python3

Preparation:

- Create virtual environment

  ```
  virtualenv .venv -p /usr/bin/python3
  ```

- Activate virtual environment

  ```
  source .venv/bin/activate
  ```

- Install python modules

  ```
  pip install -r requirements.txt
  ```

- cd to sceptre directory

  ```
  cd sceptre 
  ```

- Run sceptre, e.g. to deploy sg.yaml:

  ```
  sceptre create sg.yaml
  ```

  to delete vpc2.yaml stack:

  ```
  sceptre delete vpc2.yaml
  ```

  